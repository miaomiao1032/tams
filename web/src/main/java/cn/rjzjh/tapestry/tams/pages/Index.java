package cn.rjzjh.tapestry.tams.pages;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.EventContext;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Log;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.HttpError;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import cn.rjzjh.tapestry.busi.bean.EcAuth;
import cn.rjzjh.tapestry.busi.model.ca.CaGroupStaff;
import cn.rjzjh.tapestry.busi.model.ca.CaStaff;
import cn.rjzjh.tapestry.busi.service.IStaffService;
import cn.rjzjh.tapestry.busi.service.security.MyAuthenticationSuccessHandler;
import cn.rjzjh.tapestry.busi.service.security.UserInfo;
import cn.rjzjh.tapestry.tams.services.base.IHbService;

/**
 * Start page of application tams.
 */
@Import(stack = "easyuistack")
public class Index {
	@Inject
	private Logger logger;
	@Inject
	private Session session;
	@Inject
	private IHbService hbService;
	@Inject
	private IStaffService staffService;

	@Inject
	protected Messages messages;// 国际化
	@Inject
	private Request request;

	@CommitAfter
	public CaStaff getCaStaff() {
		CaStaff caStaff = staffService.findValidStaff(1);
		// CaStaff caStaff = hbService.findById(CaStaff.class, 1);
		Set<CaGroupStaff> group = caStaff.getCaGroupStaffs();
		for (CaGroupStaff caGroupStaff : group) {
			System.out.println(caGroupStaff.getId());
		}
		System.out.println("1=" + caStaff.getEmail());
		caStaff.setEmail("aba@175.com");
		session.persist(caStaff);
		System.out.println("2=" + caStaff.getEmail());
		return caStaff;
	}

	@Property
	private String errInfo = "";

	void onActivate() {
		// HttpSession ss= requestG.getHTTPServletRequest().getSession();
		org.apache.tapestry5.services.Session ss = request.getSession(false);
		Object excObj = ss == null ? null : ss
				.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
		AuthenticationException except = excObj == null ? null
				: (AuthenticationException) excObj;
		if (except != null) {
			logger.error("登陆错误:", except);
			if (except instanceof BadCredentialsException) {
				errInfo =messages.get("pwderror");
			} else if (except instanceof InternalAuthenticationServiceException) {
				errInfo =messages.get("nouser");
			} else if (except instanceof SessionAuthenticationException){
				errInfo =messages.get("userlogged");
			}else {
				errInfo = "系统错误";
			}
			ss.setAttribute("SPRING_SECURITY_LAST_EXCEPTION", null);
		}
	}

	@Inject
	@Symbol(SymbolConstants.CONTEXT_PATH)
	@Property
	protected String contextPath;

	/***
	 * 语言选择会触发此事件
	 *
	 * @param lang
	 */
	void onChangLocale(String lang) {
		request.getSession(true).setAttribute(
				MyAuthenticationSuccessHandler.langAttr, lang);
		System.out.println(lang);
	}

}
