package cn.rjzjh.tapestry.tams.pages.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Id;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.util.TextStreamResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.commons.util.web.EasyUiAssist;
import cn.rjzjh.tapestry.busi.constant.UnitType;
import cn.rjzjh.tapestry.busi.model.ca.CaGroup;
import cn.rjzjh.tapestry.busi.model.ca.CaOrganization;
import cn.rjzjh.tapestry.busi.service.IOrgService;
import cn.rjzjh.tapestry.component.tools.TapestryAssist;
import cn.rjzjh.tapestry.component.tools.convert.ConvertValueEnum;
import cn.rjzjh.tapestry.tams.base.ParentPage;
import cn.rjzjh.tapestry.tams.services.base.IHbService;

public class GroupManager extends ParentPage {

	@Inject
	private IOrgService orgService;

	@Inject
	private Session session;
	@Property
	private String orgTree;

	@Property
	private String orgId;

	public TextStreamResponse onQuery() {
		Criteria criteria = session.createCriteria(CaGroup.class);
		JSONObject params = TapestryAssist.packParams(request);
		String groupName = params.getString("groupName");
		JSONArray orgIds = TapestryAssist.getParams(params, "orgId");
		if (StringUtils.isNotBlank(groupName)) {
			criteria.add(Restrictions.eq("groupName", groupName));
		}
		if (orgIds != null && orgIds.length() > 0) {
			List<CaOrganization> orglist = new ArrayList<CaOrganization>();
			for (int i = 0; i < orgIds.length(); i++) {
				orglist.add(new CaOrganization(Integer.parseInt(orgIds
						.getString(i))));
			}
			criteria.add(Restrictions.in("caOrganization", orglist));
		}
		List<CaGroup> groups = criteria.list();
		Map<String, IConvertValue> conv = new HashMap<String, IConvertValue>();
		conv.put("unitType", new ConvertValueEnum(UnitType.class, messages));

		String str = EasyUiAssist.getJsonForGrid(groups, new String[] { "id",
				"groupName", "caOrganization.id,orgId",
				"caOrganization.orgName", "caOrganization.unitType,unitType" },
				conv, groups.size());
		return retStream(str);
	}

	@CommitAfter
	public TextStreamResponse onSave() {
		String id = request.getParameter("id");
		String groupName = request.getParameter("groupName");
		String orgId = request.getParameter("orgId");
		if (StringUtils.isEmpty(groupName) || StringUtils.isEmpty(orgId)) {
			return retErrorInfo("需要群组名和组织");
		}
		CaGroup saveobj = new CaGroup();
		if (StringUtils.isNotEmpty(id)) {
			saveobj.setId(Integer.parseInt(id));
		}
		saveobj.setGroupName(groupName);
		saveobj.setCaOrganization(new CaOrganization(Integer.parseInt(orgId)));
		session.saveOrUpdate(saveobj);
		return retSuccInfo();
	}

	@CommitAfter
	public TextStreamResponse onDelete() {
		String id = request.getParameter("id");
		if (StringUtils.isEmpty(id)) {
			return retErrorInfo("需要ID");
		}
		hbService.delById(CaGroup.class, Integer.parseInt(id));
		return retSuccInfo();
	}

	@SetupRender
	void init() {
		orgTree = orgService.createOrgTree(I18NConvert);
		orgId = request.getParameter("orgId");
		System.out.println("orgId=" + orgId);
	}
}
