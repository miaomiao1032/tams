package cn.rjzjh.tapestry.tams.pages.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.util.TextStreamResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.commons.util.web.EasyUiAssist;
import cn.rjzjh.tapestry.busi.constant.UnitType;
import cn.rjzjh.tapestry.busi.model.ca.CaOrganization;
import cn.rjzjh.tapestry.busi.model.ca.CaPosition;
import cn.rjzjh.tapestry.busi.service.IOrgService;
import cn.rjzjh.tapestry.component.tools.TapestryAssist;
import cn.rjzjh.tapestry.component.tools.convert.ConvertValueEnum;
import cn.rjzjh.tapestry.tams.base.ParentPage;
import cn.rjzjh.tapestry.tams.services.base.IHbService;

public class PositionManager extends ParentPage {

	@Inject
	private IOrgService orgService;
	@Inject
	private IHbService hbService;
	@Inject
	private Session session;
	@Property
	private String orgTree;

	@Property
	private String orgId;

	public TextStreamResponse onQuery() {
		Criteria criteria = session.createCriteria(CaPosition.class);
		JSONObject params = TapestryAssist.packParams(request);
		String positionName = params.getString("positionName");
		JSONArray orgIds = TapestryAssist.getParams(params, "orgId");
		if (StringUtils.isNotBlank(positionName)) {
			criteria.add(Restrictions.eq("positionName", positionName));
		}
		if (orgIds != null && orgIds.length() > 0) {
			List<CaOrganization> orglist = new ArrayList<CaOrganization>();
			for (int i = 0; i < orgIds.length(); i++) {
				orglist.add(new CaOrganization(Integer.parseInt(orgIds
						.getString(i))));
			}
			criteria.add(Restrictions.in("caOrganization", orglist));
		}
		List<CaPosition> positions = criteria.list();
		Map<String, IConvertValue> conv = new HashMap<String, IConvertValue>();
		conv.put("unitType", new ConvertValueEnum(UnitType.class, messages));

		String str = EasyUiAssist.getJsonForGrid(positions, new String[] { "id",
				"positionName", "caOrganization.id,orgId",
				"caOrganization.orgName", "caOrganization.unitType,unitType" },
				conv, positions.size());
		return retStream(str);
	}

	@CommitAfter
	public TextStreamResponse onSave() {
		String id = request.getParameter("id");
		String positionName = request.getParameter("positionName");
		String orgId = request.getParameter("orgId");
		if (StringUtils.isEmpty(positionName) || StringUtils.isEmpty(orgId)) {
			return retErrorInfo("需要职位名和组织");
		}
		CaPosition saveobj = new CaPosition();
		if (StringUtils.isNotEmpty(id)) {
			saveobj.setId(Integer.parseInt(id));
		}
		saveobj.setPositionName(positionName);
		saveobj.setCaOrganization(new CaOrganization(Integer.parseInt(orgId)));
		session.saveOrUpdate(saveobj);
		return retSuccInfo();
	}

	@CommitAfter
	public TextStreamResponse onDelete() {
		String id = request.getParameter("id");
		if (StringUtils.isEmpty(id)) {
			return retErrorInfo("需要ID");
		}
		hbService.delById(CaPosition.class, Integer.parseInt(id));
		return retSuccInfo();
	}

	@SetupRender
	void init() {
		orgTree = orgService.createOrgTree(I18NConvert);
		orgId = request.getParameter("orgId");
		System.out.println("orgId=" + orgId);
	}
}
