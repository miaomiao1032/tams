package cn.rjzjh.tapestry.tams.pages.sys;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.util.TextStreamResponse;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.commons.util.web.EasyUiAssist;
import cn.rjzjh.tapestry.busi.service.security.UserInfo;
import cn.rjzjh.tapestry.component.constant.list.Gender;
import cn.rjzjh.tapestry.component.tools.convert.ConvertValueEnum;
import cn.rjzjh.tapestry.tams.base.ParentPage;

public class OnlineUserManager extends ParentPage {

	@Inject
	private SessionRegistry sessionRegistry;

	public TextStreamResponse onQuery() {
		final String userName = request.getParameter("userName");
		List<Object> allusers = sessionRegistry.getAllPrincipals();
		if (CollectionUtils.isEmpty(allusers)) {
			return retStream(EasyUiAssist.getJsonForGridEmpty());
		}
		if (StringUtils.isNoneBlank(userName)) {
			CollectionUtils.filter(allusers, new Predicate() {
				@Override
				public boolean evaluate(Object object) {
					UserInfo userinfo = (UserInfo) object;
					return userinfo.getStaff().getUserName().equals(userName);
				}
			});
		}

		CollectionUtils.filter(allusers, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				UserInfo userinfo = (UserInfo) object;
				List<SessionInformation> sessionInformationList = sessionRegistry
						.getAllSessions(userinfo, false);
				if (CollectionUtils.isEmpty(sessionInformationList)) {
					return false;
				}
				return true;
			}
		});

		String retstr = EasyUiAssist.getJsonForGrid(allusers, new String[] {
				"staff.userName,userName", "staff.gender,gender",
				"staff.mobile,mobile", "staff.id,id" },
				new IConvertValue[] { null,
						new ConvertValueEnum(Gender.class, messages), null,
						null }, allusers.size());
		return retStream(retstr);
	}

	public TextStreamResponse onDelete() {
		String id = request.getParameter("id");
		List<Object> allusers = sessionRegistry.getAllPrincipals();
		if (CollectionUtils.isNotEmpty(allusers)) {
			for (Object object : allusers) {
				UserInfo userinfo = (UserInfo) object;
				if (userinfo.getStaff().getId() == Integer.parseInt(id)) {
					List<SessionInformation> sessionInformationList = sessionRegistry
							.getAllSessions(userinfo, false);
					if (CollectionUtils.isNotEmpty(sessionInformationList)) {
						for (SessionInformation sessioninfo : sessionInformationList) {
							sessioninfo.expireNow();
							// sessionRegistry.removeSessionInformation(sessioninfo.getSessionId());//不能加上这名,否则ConcurrentSessionFilter不能调用doLogout方法
						}
					}
					break;
				}
			}
		}
		return retSuccInfo();
	}
}
