package cn.rjzjh.tapestry.tams.pages.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.util.TextStreamResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.commons.util.web.EasyUiAssist;
import cn.rjzjh.commons.util.web.PageAssist;
import cn.rjzjh.tapestry.busi.model.ca.CaRole;
import cn.rjzjh.tapestry.busi.model.ca.CaStaff;
import cn.rjzjh.tapestry.busi.service.IOrgService;
import cn.rjzjh.tapestry.busi.service.IStaffService;
import cn.rjzjh.tapestry.component.constant.list.Gender;
import cn.rjzjh.tapestry.component.constant.list.YesOrNo;
import cn.rjzjh.tapestry.component.tools.TapestryAssist;
import cn.rjzjh.tapestry.component.tools.convert.ConvertValueEnum;
import cn.rjzjh.tapestry.tams.base.ParentPage;

public class UserManager extends ParentPage {

	@Inject
	private Session session;
	@Inject
	private IOrgService orgService;
	@Inject
	private IStaffService staffService;
	@Property
	private String orgTree;
	@Property
	private String orgId;
	@Property
	private CaStaff staff;

	public TextStreamResponse onQuery() {
		JSONObject params = TapestryAssist.packParams(request);
		String userName = TapestryAssist.getParam(params, "userName");
		Criteria criteria = session.createCriteria(CaStaff.class);
		if (StringUtils.isNotBlank(userName)) {
			criteria.add(Restrictions.eq("userName", userName));
		}
		String account = TapestryAssist.getParam(params, "account");
		if (StringUtils.isNotBlank(account)) {
			criteria.add(Restrictions.eq("account", account));
		}
		String gender = TapestryAssist.getParam(params, "gender");
		if (StringUtils.isNotBlank(gender)) {
			criteria.add(Restrictions.eq("gender", Gender.valueOf(gender)));
		}
		String isValid = TapestryAssist.getParam(params, "isValid");
		if (StringUtils.isNotBlank(isValid)) {
			criteria.add(Restrictions.eq("isValid", isValid));
		}
		String mobile = TapestryAssist.getParam(params, "mobile");
		if (StringUtils.isNotBlank(mobile)) {
			criteria.add(Restrictions.eq("mobile", mobile));
		}
		JSONArray orgIds = TapestryAssist.getParams(params, "orgId");
		if (orgIds != null && orgIds.length() > 0) {
			List<Integer> orglist = new ArrayList<Integer>();
			for (int i = 0; i < orgIds.length(); i++) {
				if(StringUtils.isNotBlank(orgIds.getString(i))){
					orglist.add(Integer.parseInt(orgIds.getString(i)));
				}
			}
			if(CollectionUtils.isNotEmpty(orglist)){
				criteria.add(Restrictions.in("caOrganization.id", orglist));
			}
		}

		PageAssist pageAssist = findByCriteriaPage(criteria);
		Map<String, IConvertValue> convertmap = new HashMap<String, IConvertValue>();
		convertmap.put("gender1", new ConvertValueEnum(Gender.class, messages));
		convertmap.put("isValid1",
				new ConvertValueEnum(YesOrNo.class, messages));

		String retstr = EasyUiAssist.getJsonForGridAlias(
				pageAssist.getResult(), new String[] { "gender,gender1",
						"isValid,isValid1", "caOrganization.id",
						"caOrganization.orgName,orgName" }, convertmap,
				pageAssist.getAllNum());
		return retStream(retstr);
	}

	@CommitAfter
	public TextStreamResponse onSave() throws Exception {
		CaStaff castaff = TapestryAssist
				.getBeanFromPage(CaStaff.class, request);
		int id = castaff.getId();
		CaStaff saveobj = null;
		if (id == 0) {
			saveobj = new CaStaff();
		} else {
			saveobj = (CaStaff) session.get(CaStaff.class, id);
		}
		if(StringUtils.isNotEmpty(castaff.getAccount())){
			saveobj.setAccount(castaff.getAccount());
		}
		if (castaff.getCaOrganization().getId() != 0) {
			saveobj.setCaOrganization(castaff.getCaOrganization());
		} else {
			saveobj.setCaOrganization(null);
		}
		saveobj.setEmail(castaff.getEmail());
		saveobj.setGender(castaff.getGender());
		if(StringUtils.isNotEmpty(castaff.getIsValid())){
			saveobj.setIsValid(castaff.getIsValid());
		}
		saveobj.setMobile(castaff.getMobile());
		saveobj.setPhone(castaff.getPhone());
		saveobj.setRemark(castaff.getRemark());
		saveobj.setUserName(castaff.getUserName());
		session.saveOrUpdate(saveobj);
		return retSuccInfo();
	}

	@SetupRender
	void init() {
		orgTree = orgService.createOrgTree(I18NConvert);
		orgId = request.getParameter("orgId");
		System.out.println("orgId=" + orgId);
	}
}
