package cn.rjzjh.tapestry.tams.pages.sys;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONArray;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.util.TextStreamResponse;
import org.hibernate.Session;

import cn.rjzjh.commons.util.web.OperateResult;
import cn.rjzjh.tapestry.busi.constant.UnitType;
import cn.rjzjh.tapestry.busi.model.ca.CaGroup;
import cn.rjzjh.tapestry.busi.model.ca.CaOrganization;
import cn.rjzjh.tapestry.busi.service.IOrgService;
import cn.rjzjh.tapestry.tams.base.ParentPage;
import cn.rjzjh.tapestry.tams.services.base.IHbService;

@Import(stack = "easyuistack")
public class OrgManager extends ParentPage {
	@Inject
	private IOrgService orgService;
	@Inject
	private IHbService hbService;
	@Inject
	private Session session;
	@Property
	private String orgTree;
	@Property
	private boolean isEmptyOrg;

	@CommitAfter
	public TextStreamResponse onCreateRoot() {
		String orgName = request.getParameter("orgName");
		if (StringUtils.isEmpty(orgName)) {
			return retErrorInfo("没有指定机构/公司名称");
		}
		CaOrganization org = new CaOrganization();
		org.setOrgName(orgName);
		org.setUnitType(UnitType.ORG.name());
		org.setOrgPath("0");
		session.save(org);
		org.setOrgPath(String.valueOf(org.getId()));
		session.update(org);
		return retSuccInfo();
	}

	/***
	 * 保存组织单元
	 *
	 * @return
	 */
	@CommitAfter
	public TextStreamResponse onSaveUnit() {
		String unitTypeStr = request.getParameter("unitType");
		String unitName = request.getParameter("unitName");
		String parentId = request.getParameter("parentId");
		if (StringUtils.isEmpty(unitTypeStr)) {
			return retErrorInfo("没有此组织类型");
		}
		OperateResult retobj = new OperateResult(1);
		JSONObject json = retobj.getJsonObj();
		JSONObject retnode = new JSONObject();
		UnitType unitType = UnitType.findByName(unitTypeStr);
		CaOrganization parentorg = (CaOrganization) session.get(CaOrganization.class, Integer.parseInt(parentId));
		switch (unitType) {
		case ORG:
		case DEPT:
			CaOrganization saveobj = new CaOrganization(unitName,
					unitType.name());
			saveobj.setCaOrganization(parentorg);
			saveobj.setOrgPath(parentorg.getOrgPath());
			session.save(saveobj);
			saveobj.setOrgPath(saveobj.getOrgPath()+","+saveobj.getId());
			session.update(saveobj);
			retnode.put("id", saveobj.getId());
			retnode.put("text", saveobj.getOrgName());
			retnode.put("iconCls", unitType.getIcon());
			retnode.put("attributes",
					new JSONObject("unitType", unitType.name(),"orgPath",saveobj.getOrgPath()));
			break;
		case GROUP:
			/*CaGroup group = new CaGroup();
			group.setGroupName(unitName);
			group.setCaOrganization(parentorg);
			session.save(group);
			retnode.put("id", group.getId());
			retnode.put("text", group.getGroupName());
			retnode.put("iconCls", unitType.getIcon());
			retnode.put("attributes",
					new JSONObject("unitType", unitType.name()));*/
			break;
		default:
			break;
		}
		json.put("retnode", retnode);
		return retStream(json.toString(true));
	}

	@CommitAfter
	public TextStreamResponse onDelUnit() {
		String orgId = request.getParameter("orgId");
		if (StringUtils.isEmpty(orgId)) {
			return retErrorInfo("需要参数:组织ID");
		}
		OperateResult opt = orgService.delOrg(Integer.parseInt(orgId));
		return retStream(opt.getJsonMsg(null));
	}

	public TextStreamResponse onLoadUnit() {
		String orgId = request.getParameter("orgId");
		JSONArray retary = new JSONArray();
		// 查找群组
		List<CaGroup> groupList = hbService
				.getQuery("from CaGroup where caOrganization.id=?")
				.setParameter(0, Integer.parseInt(orgId)).list();
		if (CollectionUtils.isNotEmpty(groupList)) {
			for (CaGroup caGroup : groupList) {
				JSONObject json = new JSONObject("id", caGroup.getId(), "text",
						caGroup.getGroupName(), "iconCls",
						UnitType.GROUP.getIcon());
				json.put("attributes", new JSONObject("unitType",
						UnitType.GROUP.name()));
				retary.put(json);
			}
		}
		// 查看职位 TODO

		return retStream(retary.toString(true));
	}

	@SetupRender
	void init() {
		orgTree = orgService.createOrgTree(I18NConvert);
		isEmptyOrg = StringUtils.isEmpty(orgTree);
	}

}
