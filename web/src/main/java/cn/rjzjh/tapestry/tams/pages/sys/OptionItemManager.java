package cn.rjzjh.tapestry.tams.pages.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.util.TextStreamResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import cn.rjzjh.commons.util.apiext.JSONUtil;
import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.commons.util.web.EasyUiAssist;
import cn.rjzjh.commons.util.web.PageAssist;
import cn.rjzjh.tapestry.busi.model.OptionItem;
import cn.rjzjh.tapestry.busi.service.ICommonService;
import cn.rjzjh.tapestry.component.constant.list.OptionGroup;
import cn.rjzjh.tapestry.component.constant.list.YesOrNo;
import cn.rjzjh.tapestry.component.tools.TapestryAssist;
import cn.rjzjh.tapestry.component.tools.convert.ConvertValueEnum;
import cn.rjzjh.tapestry.tams.base.ParentPage;
import cn.rjzjh.tapestry.tams.services.base.IHbService;

@Import(library = { "${path.jquery.plugin}/aop.min.js" })
public class OptionItemManager extends ParentPage {
	@Inject
	private Session session;
	@Inject
	private IHbService hbService;

	@Inject
	private ICommonService common;

	/*测试缓存用
	 * public OptionItem getTest() { OptionItem savobj =
	 * OptionItem.get("city-FuZhou"); savobj.setItemNameZh("抚州市");
	 * common.saveOptionItem(savobj); return savobj; }
	 */

	public TextStreamResponse onQuery() {
		Criteria criteria = session.createCriteria(OptionItem.class);
		JSONObject params = TapestryAssist.packParams(request);
		String itemCode = params.getString("itemCode");
		if (StringUtils.isNotBlank(itemCode)) {
			criteria.add(Restrictions.eq("itemCode", itemCode));
		}
		String itemName = params.getString("itemName");
		if (StringUtils.isNotBlank(itemName)) {
			String lan = supportedLocales.getCurLocale().getLanguage()
					.substring(0, 1).toUpperCase()
					+ supportedLocales.getCurLocale().getLanguage()
							.substring(1);
			criteria.add(Restrictions.eq("itemName" + lan, itemName));
		}
		OptionGroup group = OptionGroup.find(params.getString("group"));
		if (group != null) {
			criteria.add(Restrictions.eq("group", group));
		}
		YesOrNo isValid = YesOrNo.find(params.getString("isValid"));
		if (isValid != null) {
			criteria.add(Restrictions.eq("isValid", isValid));
		}

		PageAssist pageAssist = findByCriteriaPage(criteria);
		Map<String, IConvertValue> conv = new HashMap<String, IConvertValue>();
		conv.put("isValid1", new ConvertValueEnum(YesOrNo.class, messages));
		conv.put("group1", new ConvertValueEnum(OptionGroup.class, messages));
		conv.put("parentId1", optConvert);
		String str = EasyUiAssist.getJsonForGridAlias(pageAssist.getResult(),
				new String[] { "isValid,isValid1", "group,group1",
						getColLan("itemName"), "parentId,parentId1" }, conv,
				pageAssist.getAllNum());
		return retStream(str);
	}

	@CommitAfter
	public TextStreamResponse onSave() throws Exception {
		OptionItem opt = TapestryAssist.getBeanFromPage(OptionItem.class,
				request);
		if (StringUtils.isBlank(opt.getId())) {
			opt.setId(String.format("%s-%s", opt.getGroup().name(),
					opt.getItemCode()));
		}
		session.saveOrUpdate(opt);
		return retSuccInfo();
	}

	public TextStreamResponse onQueryByParent() {
		String parentId = request.getParameter("parent");
		String retStr = "[]";
		if (StringUtils.isNoneBlank(parentId)) {
			OptionGroup group = OptionGroup.find(parentId);
			if (group != null && group.getParent() != null) {
				List<OptionItem> sublist = hbService
						.getQuery("from OptionItem where group=?")
						.setParameter(0, group.getParent()).list();
				Map<String, IConvertValue> conv = new HashMap<String, IConvertValue>();
				conv.put("isValid1", new ConvertValueEnum(YesOrNo.class,
						messages));
				conv.put("group1", new ConvertValueEnum(OptionGroup.class,
						messages));
				retStr = JSONUtil.getJsonForListAlias(sublist, new String[] {
						"isValid,isValid1", "group,group1",
						getColLan("itemName") }, conv);
			}
		}
		return new TextStreamResponse("text/html", retStr);
	}
}
