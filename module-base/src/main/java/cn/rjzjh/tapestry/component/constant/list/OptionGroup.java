package cn.rjzjh.tapestry.component.constant.list;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.tapestry5.ioc.Messages;

import cn.rjzjh.commons.util.apiext.JSONUtil;
import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.tapestry.component.tools.convert.ConvertValueEnum;

public enum OptionGroup {
	wechatReply("微信快捷回复语", null, YesOrNo.yes, YesOrNo.yes), country("国家", null,
			YesOrNo.yes, YesOrNo.yes), province("省", country, YesOrNo.yes,
			YesOrNo.yes), city("市", province, YesOrNo.yes, YesOrNo.yes), region(
			"区/县", city, YesOrNo.yes, YesOrNo.yes);
	private final String desc;
	private final OptionGroup parent;// 父选项组
	private final YesOrNo isValid;// 是否可用
	private final YesOrNo isEdit;// 是否可编辑(内置状态组请设置为不可编辑)

	private OptionGroup(String desc, OptionGroup parent, YesOrNo isValid,
			YesOrNo isEdit) {
		this.desc = desc;
		this.isValid = isValid;
		this.isEdit = isEdit;
		this.parent = parent;
	}

	public String getName() {
		return this.name();
	}

	public static OptionGroup find(String name) {
		if (StringUtils.isEmpty(name)) {
			return null;
		}
		for (OptionGroup ele : OptionGroup.values()) {
			if (name.equalsIgnoreCase(ele.name())) {
				return ele;
			}
		}
		return null;
	}

	public static String buildList(Messages messages) {
		List<OptionGroup> retlist = Arrays.asList(OptionGroup.values());
		if (messages == null) {
			return JSONUtil.getJsonForListAlias(retlist);
		}
		Map<String, IConvertValue> conMap = new HashMap<String, IConvertValue>();
		conMap.put("name1", new ConvertValueEnum(OptionGroup.class, messages));
		String retstr = JSONUtil.getJsonForListAlias(retlist,
				new String[] { "name,name1" }, conMap);
		return retstr;
	}

	public YesOrNo getIsValid() {
		return isValid;
	}

	public String getDesc() {
		return desc;
	}

	public YesOrNo getIsEdit() {
		return isEdit;
	}

	public OptionGroup getParent() {
		return parent;
	}
}
