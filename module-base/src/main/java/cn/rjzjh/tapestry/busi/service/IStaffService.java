package cn.rjzjh.tapestry.busi.service;

import java.util.List;

import cn.rjzjh.tapestry.busi.constant.UnitType;
import cn.rjzjh.tapestry.busi.model.ca.CaRole;
import cn.rjzjh.tapestry.busi.model.ca.CaStaff;

/*****
 * 雇员相关服务
 *
 * @author Administrator
 *
 */
public interface IStaffService {
	/***
	 * 跟据模板查询合法的Ｉｄ
	 *
	 * @param id
	 * @return
	 */
	public List<CaStaff> findValidStaff(CaStaff caStaff);

	public CaStaff findValidStaff(int staffId);

	/***
	 * 跟据用户id得到所拥有的角色
	 *
	 * @param staffId
	 * @return
	 */
	public List<CaRole> findRolesByStaff(int staffId);

	/***
	 * 跟据组织类型得ID得到它所分配的角色
	 *
	 * @param unitType
	 *            组织类型
	 * @param objId
	 *            组织ID
	 * @param includeAncest
	 *            是否要搜索祖先组织
	 * @return
	 */
	public List<CaRole> findRolesByUnit(UnitType unitType, int objId,
			Boolean includeAncest);

	/***
	 * 找到所有合法的角色
	 *
	 * @return
	 */
	public List<CaRole> findValidRole();
}
