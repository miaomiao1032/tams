package cn.rjzjh.tapestry.busi.constant;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/***
 * 组织单位类型
 * 
 * @author Administrator
 * 
 */
public enum UnitType {
	POSITION("职位", "icon-position", new String[] {}), GROUP("群组", "icon-group",
			new String[] { "POSITION" }), DEPT("部门", "icon-dept", new String[] {
			"GROUP", "POSITION" }), ORG("机构", "icon-org", new String[] {
			"GROUP", "POSITION", "DEPT", "ORG" });//

	private final String desc;
	private final String icon;

	private final String[] sub;

	private UnitType(String desc, String icon, String[] sub) {
		this.desc = desc;
		this.sub = sub;
		this.icon = icon;
	}

	/****
	 * 通过名字返回类型，默认返回 ORG
	 * 
	 * @param name
	 * @return
	 */
	public static UnitType findByName(String name) {
		if (StringUtils.isEmpty(name)) {
			return ORG;
		}
		for (UnitType ele : UnitType.values()) {
			if (ele.name().equals(name)) {
				return ele;
			}
		}
		return ORG;
	}

	public String getDesc() {
		return desc;
	}

	public String getIcon() {
		return icon;
	}

	public UnitType[] getSub() {
		if (ArrayUtils.isNotEmpty(sub)) {
			UnitType[] rettype = new UnitType[sub.length];
			for (int i = 0; i < rettype.length; i++) {
				rettype[i] = findByName(sub[i]);
			}
			return rettype;
		}
		return new UnitType[0];
	}

}
