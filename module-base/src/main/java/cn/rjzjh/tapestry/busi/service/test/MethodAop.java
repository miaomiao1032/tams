package cn.rjzjh.tapestry.busi.service.test;
import org.springframework.stereotype.Service;

@Service
public class MethodAop implements IMethodAop{
	public String adminMessage() {
		return "admin message";
	}

	public String userMessage() {
		return "user message";
	}

}
