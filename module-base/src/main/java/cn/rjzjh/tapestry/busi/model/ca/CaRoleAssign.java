package cn.rjzjh.tapestry.busi.model.ca;

// Generated 2014-9-6 10:25:55 by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * CaRoleAssign generated by hbm2java
 */
@Entity
@Table(name = "CA_RoleAssign")
public class CaRoleAssign implements java.io.Serializable {

	private int id;
	private CaRole caRole;
	private String assignType;
	private Integer assignValue;

	public CaRoleAssign() {
	}

	public CaRoleAssign(int id) {
		this.id = id;
	}

	public CaRoleAssign(int id, CaRole caRole, String assignType,
			Integer assignValue) {
		this.id = id;
		this.caRole = caRole;
		this.assignType = assignType;
		this.assignValue = assignValue;
	}

	@Id()
	@Column(name = "id", unique = true, nullable = false, insertable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId")
	public CaRole getCaRole() {
		return this.caRole;
	}

	public void setCaRole(CaRole caRole) {
		this.caRole = caRole;
	}

	@Column(name = "AssignType", length = 10)
	public String getAssignType() {
		return this.assignType;
	}

	public void setAssignType(String assignType) {
		this.assignType = assignType;
	}

	@Column(name = "AssignValue")
	public Integer getAssignValue() {
		return this.assignValue;
	}

	public void setAssignValue(Integer assignValue) {
		this.assignValue = assignValue;
	}

}
