package cn.rjzjh.tapestry.busi.service.security;

import java.util.Collection;
import java.util.List;
import org.apache.tapestry5.ioc.internal.util.CollectionFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import cn.rjzjh.tapestry.busi.model.ca.CaRole;
import cn.rjzjh.tapestry.busi.model.ca.CaStaff;

public class UserInfo extends User {
	private static final long serialVersionUID = 1L;
	private final CaStaff staff;
	private final List<CaRole> roles = CollectionFactory.newList();
	private boolean isDev = false;
	private String lang = "zh";// 选择的语言

	public UserInfo(CaStaff staff,
			Collection<? extends GrantedAuthority> authorities) {
		super(staff.getUserName(), staff.getPassword(), true, true, true, true,
				authorities);// 需要扩展staff表
		this.staff = staff;
	}

	public CaStaff getStaff() {
		return this.staff;
	}

	public boolean isDev() {
		return isDev;
	}

	public void setDev(boolean isDev) {
		this.isDev = isDev;
	}

	public List<CaRole> getRoles() {
		return roles;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
}
