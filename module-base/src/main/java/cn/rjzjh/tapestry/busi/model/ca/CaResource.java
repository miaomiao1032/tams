package cn.rjzjh.tapestry.busi.model.ca;

// Generated 2014-9-6 10:25:55 by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * CaResource generated by hbm2java
 */
@Entity
@Table(name = "CA_Resource")
public class CaResource implements java.io.Serializable {

	private int id;
	private CaResType caResType;
	private CaResource caResource;
	private String resCode;
	private String resName;
	private String resValue;
	private String icon;
	private Integer resLevel;
	private String canEdit;
	private Integer showOrder;
	private String remark;
	private String bound1;
	private String bound2;
	private String bound3;
	private String bound4;
	private String bound5;
	private String bound6;
	private String bound7;
	private String bound8;
	private String bound9;
	private Set<CaResTypeDict> caResTypeDicts = new HashSet<CaResTypeDict>(0);
	private Set<CaResource> caResources = new HashSet<CaResource>(0);
	private Set<CaRoleRes> caRoleReses = new HashSet<CaRoleRes>(0);

	public CaResource() {
	}

	public CaResource(int id) {
		this.id = id;
	}

	public CaResource(int id, CaResType caResType, String resName,
			String resValue) {
		this.id = id;
		this.caResType = caResType;
		this.resName = resName;
		this.resValue = resValue;
	}

	public CaResource(int id, CaResType caResType, CaResource caResource,
			String resCode, String resName, String resValue, String icon,
			Integer resLevel, String canEdit, Integer showOrder, String remark,
			String bound1, String bound2, String bound3, String bound4,
			String bound5, String bound6, String bound7, String bound8,
			String bound9, Set<CaResTypeDict> caResTypeDicts,
			Set<CaResource> caResources, Set<CaRoleRes> caRoleReses) {
		this.id = id;
		this.caResType = caResType;
		this.caResource = caResource;
		this.resCode = resCode;
		this.resName = resName;
		this.resValue = resValue;
		this.icon = icon;
		this.resLevel = resLevel;
		this.canEdit = canEdit;
		this.showOrder = showOrder;
		this.remark = remark;
		this.bound1 = bound1;
		this.bound2 = bound2;
		this.bound3 = bound3;
		this.bound4 = bound4;
		this.bound5 = bound5;
		this.bound6 = bound6;
		this.bound7 = bound7;
		this.bound8 = bound8;
		this.bound9 = bound9;
		this.caResTypeDicts = caResTypeDicts;
		this.caResources = caResources;
		this.caRoleReses = caRoleReses;
	}

	@Id()
	@Column(name = "id", unique = true, nullable = false, insertable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "resType", nullable = false)
	public CaResType getCaResType() {
		return this.caResType;
	}

	public void setCaResType(CaResType caResType) {
		this.caResType = caResType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentId")
	public CaResource getCaResource() {
		return this.caResource;
	}

	public void setCaResource(CaResource caResource) {
		this.caResource = caResource;
	}

	@Column(name = "resCode", length = 2000)
	public String getResCode() {
		return this.resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	@Column(name = "resName", nullable = false, length = 100)
	public String getResName() {
		return this.resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	@Column(name = "resValue", nullable = false, length = 2000)
	public String getResValue() {
		return this.resValue;
	}

	public void setResValue(String resValue) {
		this.resValue = resValue;
	}

	@Column(name = "icon", length = 32)
	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Column(name = "resLevel")
	public Integer getResLevel() {
		return this.resLevel;
	}

	public void setResLevel(Integer resLevel) {
		this.resLevel = resLevel;
	}

	@Column(name = "canEdit")
	public String getCanEdit() {
		return this.canEdit;
	}

	public void setCanEdit(String canEdit) {
		this.canEdit = canEdit;
	}

	@Column(name = "showOrder")
	public Integer getShowOrder() {
		return this.showOrder;
	}

	public void setShowOrder(Integer showOrder) {
		this.showOrder = showOrder;
	}

	@Column(name = "remark", length = 2000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "bound1", length = 2000)
	public String getBound1() {
		return this.bound1;
	}

	public void setBound1(String bound1) {
		this.bound1 = bound1;
	}

	@Column(name = "bound2", length = 2000)
	public String getBound2() {
		return this.bound2;
	}

	public void setBound2(String bound2) {
		this.bound2 = bound2;
	}

	@Column(name = "bound3", length = 2000)
	public String getBound3() {
		return this.bound3;
	}

	public void setBound3(String bound3) {
		this.bound3 = bound3;
	}

	@Column(name = "bound4", length = 2000)
	public String getBound4() {
		return this.bound4;
	}

	public void setBound4(String bound4) {
		this.bound4 = bound4;
	}

	@Column(name = "bound5", length = 2000)
	public String getBound5() {
		return this.bound5;
	}

	public void setBound5(String bound5) {
		this.bound5 = bound5;
	}

	@Column(name = "bound6", length = 2000)
	public String getBound6() {
		return this.bound6;
	}

	public void setBound6(String bound6) {
		this.bound6 = bound6;
	}

	@Column(name = "bound7", length = 2000)
	public String getBound7() {
		return this.bound7;
	}

	public void setBound7(String bound7) {
		this.bound7 = bound7;
	}

	@Column(name = "bound8", length = 2000)
	public String getBound8() {
		return this.bound8;
	}

	public void setBound8(String bound8) {
		this.bound8 = bound8;
	}

	@Column(name = "bound9", length = 2000)
	public String getBound9() {
		return this.bound9;
	}

	public void setBound9(String bound9) {
		this.bound9 = bound9;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caResource")
	public Set<CaResTypeDict> getCaResTypeDicts() {
		return this.caResTypeDicts;
	}

	public void setCaResTypeDicts(Set<CaResTypeDict> caResTypeDicts) {
		this.caResTypeDicts = caResTypeDicts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caResource")
	public Set<CaResource> getCaResources() {
		return this.caResources;
	}

	public void setCaResources(Set<CaResource> caResources) {
		this.caResources = caResources;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caResource", cascade = { CascadeType.REMOVE })
	public Set<CaRoleRes> getCaRoleReses() {
		return this.caRoleReses;
	}

	public void setCaRoleReses(Set<CaRoleRes> caRoleReses) {
		this.caRoleReses = caRoleReses;
	}

	@Override
	public boolean equals(Object obj) {
		CaResource tmepObj = (CaResource) obj;
		return this.resValue.equalsIgnoreCase(tmepObj.getResValue())
				&& this.caResType.getId() == tmepObj.getCaResType().getId();
	}

	@Override
	public int hashCode() {
		return (this.resValue + "_" + this.caResType.getId()).hashCode();
	}

}
