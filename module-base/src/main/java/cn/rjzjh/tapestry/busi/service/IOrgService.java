package cn.rjzjh.tapestry.busi.service;

import java.util.List;

import cn.rjzjh.commons.util.assistbean.EasyUINode;
import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.commons.util.web.OperateResult;
import cn.rjzjh.tapestry.busi.model.ca.CaOrganization;

/****
 * 组织相关的服务
 * 
 * @author Administrator
 * 
 */
public interface IOrgService {
	/****
	 * 找到所有组织的树
	 * 
	 * @param i18NConvert
	 * @return
	 */
	public String createOrgTree(IConvertValue i18NConvert);

	/****
	 * 删除组织,如果有子组织不能删除,删除组织要做的动作: 1/删除此组织下所有群组 2/删除此组织下所有职位
	 * 3/把挂到此组织下的用户orgid设置为NULL
	 * 
	 * @param orgid
	 * @return
	 */
	public OperateResult delOrg(Integer orgid);

	/***
	 * 通过orgid得到它所有的祖先节点
	 * 
	 * @param orgid
	 * @return
	 */
	public List<CaOrganization> findAncestOrg(int orgid);
}
