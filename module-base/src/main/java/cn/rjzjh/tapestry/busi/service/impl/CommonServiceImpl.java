package cn.rjzjh.tapestry.busi.service.impl;

import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.rjzjh.tapestry.busi.model.OptionItem;
import cn.rjzjh.tapestry.busi.service.ICommonService;
import cn.rjzjh.tapestry.tams.services.base.IHbService;

@Service("common")
@SuppressWarnings("unchecked")
public class CommonServiceImpl implements ICommonService {
	@Autowired
	@Inject
	private IHbService hbService;

	@Autowired
	@Inject
	private Session session;

	@Override
	public IHbService getHb() {
		return this.hbService;
	}

	/****
	 * 将会由缓存管理
	 */
	@Override
	public void saveOptionItem(OptionItem saveobj) {
		hbService.saveOrUpdate(saveobj);
	}

}
