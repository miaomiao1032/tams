package cn.rjzjh.tapestry.busi.service;

import java.util.List;

import cn.rjzjh.commons.util.assistbean.EasyUINode;
import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.tapestry.busi.model.ca.CaResType;
import cn.rjzjh.tapestry.busi.model.ca.CaResource;
import cn.rjzjh.tapestry.busi.model.ca.CaRole;

/****
 * 菜单相关服务
 * 
 * @author Administrator
 * 
 */
public interface IMenuService {
	/****
	 * 跟据角色查到所拥有的节点，由于要查孙子节点，不能传入moduleId进行查询
	 * 
	 * @param roles
	 * @param isRoot
	 *            true:只查询模块 false:查询模块以下的所有菜单
	 * @return
	 */
	public List<CaResource> findAllModule(List<CaRole> roles, boolean isRoot);

	/***
	 * 查询指定模块下面指定角色所有子节节点
	 * 
	 * @param moduleId
	 * @param roles
	 * @param I18NConvert
	 * @return
	 */
	public List<EasyUINode> findModuleMenuForRoles(int moduleId,
			List<CaRole> roles, IConvertValue I18NConvert);

	/*****
	 * 查询所有模块及其子菜单，
	 * 
	 * @param i18NConvert
	 * @param hasUnallot
	 *            是否需要“未分配的资源”虚拟模块
	 * @return
	 */
	public List<EasyUINode> findAllModuleMenu(IConvertValue i18NConvert,
			boolean hasUnallot);

	/***
	 * 找出所有可以编辑的资源类型
	 * 
	 * @return
	 */
	public List<CaResType> findCanEditResType();

	/**
	 * 查找需要同步的资源
	 * 
	 * @return
	 */
	public List<CaResource> findSyncRes();

}
