package cn.rjzjh.tapestry.busi.service;

import org.apache.tapestry5.hibernate.annotations.CommitAfter;

import cn.rjzjh.tapestry.busi.model.OptionItem;
import cn.rjzjh.tapestry.tams.services.base.IHbService;

/***
 * 公共业务服务,注意Spring不能拥有事务
 *
 * @author Administrator
 *
 */
public interface ICommonService {
	/***
	 * 得到内置的Hibernate服务
	 *
	 * @return
	 */
	public IHbService getHb();

    public void saveOptionItem(OptionItem saveobj);
}
