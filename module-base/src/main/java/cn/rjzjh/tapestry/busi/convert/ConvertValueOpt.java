package cn.rjzjh.tapestry.busi.convert;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import cn.rjzjh.commons.util.apiext.StringUtil;
import cn.rjzjh.commons.util.callback.IConvertValue;
import cn.rjzjh.tapestry.busi.model.OptionItem;
import cn.rjzjh.tapestry.component.services.ISupportedLocales;

public class ConvertValueOpt implements IConvertValue {
	private final String lan;

	public ConvertValueOpt(ISupportedLocales supportedLocales) {
		String curLan = supportedLocales.getCurLocale().getLanguage();
		this.lan = curLan.substring(0, 1).toUpperCase() + curLan.substring(1);
	}

	@Override
	public String getStr(String key) {
		if (StringUtil.isNull(key)) {
			return "";
		}

		OptionItem opt = OptionItem.get(key);
		try {
			return BeanUtils.getSimpleProperty(opt,
					String.format("itemName%s", lan));
		} catch (Exception e) {
			return key;
		}
	}

}
