package cn.rjzjh.tapestry.busi.bean;

import java.util.List;

import org.apache.tapestry5.ioc.internal.util.CollectionFactory;

import cn.rjzjh.tapestry.busi.model.ca.CaRole;
import cn.rjzjh.tapestry.busi.model.ca.CaStaff;
import cn.rjzjh.tapestry.busi.service.security.UserInfo;

/****
 * 放Session中的对象
 * 
 * @author Administrator
 * 
 */
public class EcAuth {
	private final UserInfo userInfo;

	public EcAuth(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	private boolean isLocked; // 当前用户是否处于锁屏状态

	public List<CaRole> hasRoles() {
		return userInfo.getRoles();
	}

	public CaStaff getStaff() {
		return userInfo.getStaff();
	}

	public boolean isDev() {
		return userInfo.isDev();
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}
}
