package cn.rjzjh.tapestry.tams.services.base;

import java.io.Serializable;
import java.util.List;

import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Example.PropertySelector;

/****
 * Hibernate辅助类,拥有事务
 *
 * @author Administrator
 *
 */
public interface IHbService {

	public <T extends Serializable> T findById(Class clazz, Serializable id);

	/****
	 * Oracle/mysql 支持悲观锁的会用　select * from XXX for update 锁记录，其它数据库同 attachQuery
	 *
	 * @param entity
	 */
	public <T extends Serializable> void attachLock(T entity);

	public Query getQuery(String hql);

	public Query getQuerySQL(String sql);

	/***
	 * 通过示例查询，注意它会忽略　主键
	 *
	 * @param entity
	 * @param selector
	 * @return
	 */
	public <T extends Serializable> List<T> findByExample(T entity,
			PropertySelector selector);

	/***
	 * 通过示例查询，注意它会忽略　主键
	 *
	 * @param entity
	 * @param excludes
	 * @return
	 */
	public <T extends Serializable> List<T> findByExample(T entity,
			String... excludes);

	public <T extends Serializable> List<T> findByExamplePage(T entity,
			int pageNo, int pageSize, String... excludes);

	public <T extends Serializable> List<T> findByQueryPage(Query query,
			int pageNo, int pageSize);

	public <T extends Serializable> List<T> findByHqlPage(String hql,
			int pageNo, int pageSize);

	public <T extends Serializable> List<T> findByCriteriaPage(
			Criteria criteria, int pageNo, int pageSize);

	@CommitAfter
	void delById(Class clazz, Serializable entityId);

	@CommitAfter
	public <T extends Serializable> void saveOrUpdate(T obj);
}
